#
# Description: <Method description here>
#

def get_locks(datastore="semaphores-list",path_name="semaphores_list_1/lock_list")
  lock_path="/#{datastore}/#{path_name}"
  locks=$evm.instance_find(lock_path+"/*")
  $evm.log(:info,"lock_path=#{lock_path+"/*"} locks=#{locks.inspect}")
  return locks
end

def get_lock_value(lock_name,datastore="semaphores-list",path_name="semaphores_list_1/lock_list")
  lock_path="/#{datastore}/#{path_name}/#{lock_name}"
  lock_hash=$evm.instance_get(lock_path)
  $evm.log(:info,"lock_hash=#{lock_hash}")
  return { lock_name => lock_hash } unless lock_hash.blank?
end 

def wait_lock(lock_name, ttl=2, description="no desc", datastore="semaphores-list",path_name="semaphores_list_1/lock_list")
  result=false
  date=DateTime.now
  id=$evm.get_state_var("#{lock_name}_UID")
  if id.blank?
    id=lock_name+"-#{Time.now.to_f}"
    $evm.set_state_var("#{lock_name}_UID",id)
  end
  
#  id=$evm.root["service_id"] rescue nil|| "azerty"
  data={"locker_unic_id" =>id, "creation_date" => date, "ttl" => ttl, "description" => description}
  #wait for lock free
  lock_path="/#{datastore}/#{path_name}/#{lock_name}"
  lock_hash=get_lock_value(lock_name,datastore,path_name)
  
  unless $evm.instance_exists?lock_path
     $evm.log(:info,"<wait_lock> creating lock #{lock_path} with #{data}")
    $evm.instance_create(lock_path,data) 
  else
    locker_unic_id=lock_hash[lock_name]["locker_unic_id"] rescue nil
    $evm.log(:info,"<wait_lock> #{lock_path} exists locker_unic_id=#{locker_unic_id}")

    case locker_unic_id
      when nil
        $evm.instance_update("#{lock_path}",data)
        $evm.log(:info,"<wait_lock> update #{lock_path} with #{data}")
      when id
        result=true
        $evm.log(:info,"<wait_lock>  #{lock_path} is already for me with #{data}")
      else
        age=(date.to_i-(DateTime.parse(lock_hash[lock_name]["creation_date"]) rescue 0).to_i)/60
        ttl=lock_hash[lock_name]["ttl"].to_i
        $evm.log(:info,"<wait_lock> #{lock_path} is locked for an other with #{lock_hash[lock_name]} age=#{age} ttl=#{ttl}")
        if age > ttl
          $evm.log(:info,"<wait_lock> #{lock_path} too old (#{age} > #{ttl} minutes): cleaning")
          delete_lock(lock_name,datastore,path_name)
        end
    end
  end
  return result
end

def delete_lock(lock_name,datastore="semaphores-list",path_name="semaphores_list_1/lock_list")
  result=false
  lock_path="/#{datastore}/#{path_name}/#{lock_name}"
  lock_hash=get_lock_value(lock_name,datastore,path_name)
  lock_val=lock_hash[lock_name]["locker_unic_id"] rescue nil
  $evm.log(:info,"lock_path=#{lock_path} lock_val=#{lock_val}")


  if lock_hash.nil?
    result=true 
  else
    result=true
    $evm.instance_delete("#{lock_path}")
  end
  return result
end
