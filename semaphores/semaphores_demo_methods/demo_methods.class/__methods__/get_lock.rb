#
# Description: <Method description here>
#

$evm.log(:info,"######################################################")

$evm.root.attributes.each{|k,v| $evm.log(:info,"ROOT[#{k}]=#{v}")}
$evm.root["ae_result"]="retry"
$evm.root["ae_retry_interval"]=10.seconds

datastore="/semaphores"
path_name=datastore+"/semaphores/lock_name"
lock_name="lock1"
lock_path="#{path_name}/#{lock_name}"

#id=prov.id || "azerty"
id=$evm.root["service_id"] || "azerty"
    data={"locker_id" =>id}
#wait for lock free
lock_hash=$evm.instance_get(lock_path)
$evm.log(:info,"lock_hash=#{lock_hash}")

$evm.instance_create(lock_path,data)
lock_val=lock_hash["locker_id"] rescue nil
$evm.log(:info,"lock_val=#{lock_val}")

case lock_val
  when nil
    $evm.instance_update("#{lock_path}",data)
    $evm.log(:info," update #{lock_path} with #{data}")
  when id
    $evm.root["ae_result"]="ok"
    $evm.log(:info," is already #{lock_path} with #{data}")
  else
    $evm.log(:info," not set #{lock_path} with #{data}")
end
##lock is #{id}? => exit OK
##lock is nil => allocate => exit retry

#allocate lock

