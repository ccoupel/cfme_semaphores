# Description: <Method description here>
#
#$evm.root.attributes.sort.each{|k,v| $evm.log(:info,"ROOT[#{k}]=#{v}")}
lock_name=$evm.root["lock_name"]
description=$evm.root["lock_description"]
datastore=$evm.root["lock_datastore"]
path_name=$evm.root["lock_path"]

$evm.log(:info,"<release_lock> releasing...")
delete_lock(lock_name,datastore,path_name)
