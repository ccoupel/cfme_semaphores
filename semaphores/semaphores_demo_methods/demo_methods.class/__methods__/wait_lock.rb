#
# Description: Wait until "lock_name" is notfree
#
#$evm.root.attributes.sort.each{|k,v| $evm.log(:info,"ROOT[#{k}]=#{v}")}

lock_name=$evm.root["lock_name"]
description=$evm.root["lock_description"]
datastore=$evm.root["lock_datastore"]
path_name=$evm.root["lock_path"]
ttl=$evm.root["lock_ttl"] || 2


is_me=wait_lock(lock_name, ttl, description, datastore,path_name)
$evm.log(:info,"<MUTEX> #{lock_name} in /#{datastore}#{path_name} set for me: #{is_me}")
$evm.root['ae_result'] = 'retry'
$evm.root["ae_retry_interval"]=10.seconds

$evm.root["ae_result"]="ok" if is_me
