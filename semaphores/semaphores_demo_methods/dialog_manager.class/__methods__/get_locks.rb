#
# Description: <Method description here>
#

$evm.root.attributes.each{ |k,v| $evm.log(:info, "default ROOT[#{k}]=#{v}")}

value=$evm.root["dialog_locks"]
$evm.log(:info,"default=#{value}")
locks=get_locks($evm.root["dialog_datastore"],$evm.root["dialog_path"])
values=locks.map{|k,v| [k,k]}.to_h
$evm.root["values"]=values
$evm.root["default_value"]=value
